package com.santigarcia.warehouseinventoryapi.util.exception.handler;

import com.santigarcia.warehouseinventoryapi.controller.TrackedDeviceController;
import com.santigarcia.warehouseinventoryapi.util.exception.DeviceAlreadyExistsException;
import com.santigarcia.warehouseinventoryapi.util.exception.ResourceNotFoundException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TrackedDeviceExceptionHandlerTest {
    @InjectMocks
    private TrackedDeviceExceptionHandler exceptionHandler;
    @Mock
    private DeviceAlreadyExistsException deviceAlreadyExistsException;
    @Mock
    private MethodArgumentNotValidException methodArgumentNotValidException;
    @Mock
    private ResourceNotFoundException resourceNotFoundException;
    private FieldError error1;
    private FieldError error2;
    private List<FieldError> fieldErrors;
    @BeforeEach
    public void setUp(){
        exceptionHandler = new TrackedDeviceExceptionHandler();
        deviceAlreadyExistsException = mock(DeviceAlreadyExistsException.class);
        methodArgumentNotValidException = mock(MethodArgumentNotValidException.class);
        resourceNotFoundException = mock(ResourceNotFoundException.class);
        fieldErrors = new ArrayList<>();
        error1 =
                new FieldError("device", "pinCode", "Pin code cannot be empty");
        error2 =
                new FieldError("device", "pinCode", "Pin code must be a 7-digit number");
        fieldErrors.add(error1);
        fieldErrors.add(error2);
    }
    @Test
    public void testHandleDeviceAlreadyExistException() {
        String errorMessage = "Device already exists";
        when(deviceAlreadyExistsException.getMessage()).thenReturn(errorMessage);
        ResponseEntity<String> response
                = exceptionHandler.handleDeviceAlreadyExistException(deviceAlreadyExistsException);
        assertEquals(errorMessage, response.getBody());
        assertEquals(400, response.getStatusCodeValue());
    }
    @Test
    void testHandleResourceNotFoundExceptionException() {
        String errorMessage = "Resource not found";
        when(resourceNotFoundException.getMessage()).thenReturn(errorMessage);
        ResponseEntity<String> response
                = exceptionHandler.handleResourceNotFoundExceptionException(resourceNotFoundException);
        assertEquals(errorMessage, response.getBody());
        assertEquals(400, response.getStatusCodeValue());
    }
}