package com.santigarcia.warehouseinventoryapi.service.impl;

import com.santigarcia.warehouseinventoryapi.model.dao.TrackedDeviceDAO;
import com.santigarcia.warehouseinventoryapi.repository.TrackedDeviceRepository;
import com.santigarcia.warehouseinventoryapi.model.entity.TrackedDeviceEntity;
import com.santigarcia.warehouseinventoryapi.util.exception.DeviceAlreadyExistsException;
import com.santigarcia.warehouseinventoryapi.util.exception.ResourceNotFoundException;
import com.santigarcia.warehouseinventoryapi.util.exception.trackeddeviceutils.TrackedDeviceUtils;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

import org.junit.jupiter.api.Test;
import org.springframework.dao.EmptyResultDataAccessException;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * DeviceConfigurationServiceImplTest is a test class for DeviceConfigurationServiceImpl
 */
class DeviceConfigurationServiceImplTest {
    @Mock
    private TrackedDeviceRepository trackedDeviceRepository;
    @InjectMocks
    private DeviceConfigurationServiceImpl deviceConfigurationService;
    private TrackedDeviceEntity existingDevice;
    private TrackedDeviceDAO trackedDeviceDAO;
    private BigInteger pinCode;
    private Long id;

    /**
     * Sets up the initial configuration for each test case.
     */
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        pinCode = new BigInteger("1234567890");
        id = 1L;
        existingDevice = TrackedDeviceEntity.builder()
                .id(id)
                .pinCode(pinCode)
                .status(TrackedDeviceUtils.READY_STATUS_INT)
                .temperature(-1)
                .build();

        trackedDeviceDAO = TrackedDeviceDAO.builder()
                .id(id)
                .pinCode(pinCode.toString())
                .status(TrackedDeviceUtils.READY_STATUS)
                .temperature(-1)
                .build();
    }

    /**
     * Tests the createTrackedDevice method.
     * It checks that a new tracked device is correctly saved
     *  when there is no other device with the same pin code.
     */
    @Test
    public void testCreateTrackedDevice() {
        when(trackedDeviceRepository.findByPinCode(pinCode)).thenReturn(null);
        when(trackedDeviceRepository.save(any(TrackedDeviceEntity.class))).thenReturn(existingDevice);
        TrackedDeviceDAO result = deviceConfigurationService.createTrackedDevice(trackedDeviceDAO);

        assertNotNull(result);
        assertEquals(id, result.getId());
        assertEquals(pinCode.toString(), result.getPinCode());
        assertEquals(TrackedDeviceUtils.READY_STATUS, result.getStatus());
        assertEquals(-1, result.getTemperature());
    }

    /**
     * Tests the createTrackedDevice method.
     * It checks that a DeviceAlreadyExistsException is thrown
     *  when trying to save a tracked device with an existing pin code.
     */
    @Test
    public void testCreateTrackedDeviceWithExistingPinCode() {
        when(trackedDeviceRepository.findByPinCode(pinCode)).thenReturn(existingDevice);
        assertThrows(DeviceAlreadyExistsException.class, () -> {
            deviceConfigurationService.createTrackedDevice(trackedDeviceDAO);
        });
    }

    /**
     * Tests the updateTrackedDevice method.
     * It checks that a tracked device is updated correctly when the device exists.
     */
    @Test
    public void testUpdateTrackedDevice() {
        when(trackedDeviceRepository.findById(id)).thenReturn(Optional.of(existingDevice));
        when(trackedDeviceRepository.save(existingDevice)).thenReturn(existingDevice);
        TrackedDeviceDAO result = deviceConfigurationService.updateTrackedDevice(id);
        assertNotNull(result);
        assertEquals(id, result.getId());
        assertEquals(pinCode.toString(), result.getPinCode());
        assertEquals(TrackedDeviceUtils.ACTIVE_STATUS, result.getStatus());
    }

    /**
     * Tests the updateTrackedDevice method.
     * It checks that a ResourceNotFoundException is thrown
     *  when trying to update a tracked device that doesn't exist.
     */
    @Test
    public void testUpdateTrackedDeviceNotFound() {
        when(trackedDeviceRepository.findById(id)).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> {
            deviceConfigurationService.updateTrackedDevice(id);
        });
    }

    /**
     * Tests the getAllTrackedDevices method.
     * It checks that all tracked devices are returned in the correct order
     *  when there is at least one device saved.
     */
    @Test
    public void testGetAllTrackedDevices() {
        List<TrackedDeviceEntity> trackedDeviceEntities = new ArrayList<>();
        trackedDeviceEntities.add(existingDevice);
        when(trackedDeviceRepository.findAllOrderByPinCodeAsc()).thenReturn(trackedDeviceEntities);
        List<TrackedDeviceDAO> result = deviceConfigurationService.getAllTrackedDevices();

        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(id, result.get(0).getId());
        assertEquals(pinCode.toString(), result.get(0).getPinCode());
        assertEquals(TrackedDeviceUtils.READY_STATUS, result.get(0).getStatus());
        assertEquals(-1, result.get(0).getTemperature());
    }

    /**
     * Test getTrackedDeviceById method.
     *  Which returns a TrackedDeviceDAO object for the given id.
     */
    @Test
    public void testGetTrackedDeviceById() {
        when(trackedDeviceRepository.findById(existingDevice.getId())).thenReturn(Optional.of(existingDevice));
        TrackedDeviceDAO result = deviceConfigurationService.getTrackedDeviceById(existingDevice.getId());
        assertEquals(trackedDeviceDAO, result);
    }

    /**
     * Test getTrackedDeviceById method
     *  which throws a ResourceNotFoundException when the given id is not found.
     */
    @Test
    public void testGetTrackedDeviceByIdNotFound() {
        Long nonExistentId = 999L;
        when(trackedDeviceRepository.findById(nonExistentId)).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> deviceConfigurationService.getTrackedDeviceById(nonExistentId));
    }

    /**
     * Test deleteTrackedDevice method which deletes a tracked device with the given id.
     */
    @Test
    public void testDeleteTrackedDevice() {
        Long deviceIdToDelete = existingDevice.getId();
        doNothing().when(trackedDeviceRepository).deleteById(deviceIdToDelete);
        deviceConfigurationService.deleteTrackedDevice(deviceIdToDelete);
        Mockito.verify(trackedDeviceRepository, Mockito.times(1)).deleteById(deviceIdToDelete);
    }

    /**
     * Test deleteTrackedDevice method
     *  which throws a ResourceNotFoundException when the given id is not found.
     */
    @Test
    public void testDeleteTrackedDeviceNotFound() {
        Long nonExistentId = 999L;
        doThrow(new EmptyResultDataAccessException(1)).when(trackedDeviceRepository).deleteById(nonExistentId);
        assertThrows(
                ResourceNotFoundException.class,
                () -> deviceConfigurationService.deleteTrackedDevice(nonExistentId));
        Mockito.verify(trackedDeviceRepository, Mockito.times(1)).deleteById(nonExistentId);
    }
}