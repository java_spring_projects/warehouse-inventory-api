package com.santigarcia.warehouseinventoryapi.model.dao;

import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This class contains tests for the TrackedDeviceDAO class.
 */
@ExtendWith(MockitoExtension.class)
class TrackedDeviceDAOTest {
    @InjectMocks
    private TrackedDeviceDAO trackedDevice;
    private static Validator validator;
    private TrackedDeviceDAO device1;
    private TrackedDeviceDAO device2;

    /**
     * Set up the validator factory before all tests.
     */
    @BeforeAll
    public static void setupValidator() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    /**
     * Set up the tracked device and other devices before each test.
     */
    @BeforeEach
    public void setUp(){
        trackedDevice = TrackedDeviceDAO.builder()
                .id(1L)
                .pinCode("1234567")
                .status("ACTIVE")
                .temperature(5)
                .build();

        device1 = TrackedDeviceDAO.builder()
                .id(1L)
                .pinCode("1234567")
                .status("active")
                .temperature(25)
                .build();

        device2 = TrackedDeviceDAO.builder()
                .id(2L)
                .pinCode("7654321")
                .status("inactive")
                .temperature(15)
                .build();
    }

    /**
     * Test the creation of a tracked device and its attributes.
     */
    @Test
    public void testCreateTrackedDevice() {
        Assertions.assertNotNull(trackedDevice.getId());
        Assertions.assertEquals("1234567", trackedDevice.getPinCode());
        Assertions.assertEquals("ACTIVE", trackedDevice.getStatus());
        Assertions.assertEquals(5, trackedDevice.getTemperature());
    }

    /**
     * Test the @NotEmpty and @Pattern annotations on the pinCode field with a valid input.
     */
    @Test
    public void testValidPinCode() {
        assertTrue(validator.validate(trackedDevice).isEmpty());
    }

    /**
     * Test the @NotEmpty annotation on the pinCode field with null input.
     */
    @Test
    public void testNullPinCode() {
        trackedDevice.setPinCode(null);
        assertFalse(validator.validate(trackedDevice).isEmpty());
    }

    /**
     * Test the @NotEmpty annotation on the pinCode field with empty input.
     */
    @Test
    public void testEmptyPinCode() {
        trackedDevice.setPinCode("");
        assertFalse(validator.validate(trackedDevice).isEmpty());
    }

    /**
     * Test the @Pattern annotation on the pinCode field with invalid input.
     */
    @Test
    public void testInvalidPinCode() {
        trackedDevice.setPinCode("123");
        assertFalse(validator.validate(trackedDevice).isEmpty());
    }
}