package com.santigarcia.warehouseinventoryapi.controller;

import com.santigarcia.warehouseinventoryapi.model.dao.TrackedDeviceDAO;
import com.santigarcia.warehouseinventoryapi.service.DeviceConfigurationService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.junit.jupiter.api.BeforeEach;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * This class contains tests for the TrackedDeviceController class.
 */
@ExtendWith(MockitoExtension.class)
class TrackedDeviceControllerTest {
    @Mock
    private DeviceConfigurationService deviceConfigurationService;
    @InjectMocks
    private TrackedDeviceController trackedDeviceController;
    private List<TrackedDeviceDAO> deviceList;
    private TrackedDeviceDAO device1;
    private TrackedDeviceDAO device2;

    /**
     * Sets up the necessary variables for testing before each test method.
     * This method initializes the deviceList, device1, and device2 variables.
     */
    @BeforeEach
    public void setUp() {
        deviceList = new ArrayList<>();
        device1 = TrackedDeviceDAO.builder().id(1L).pinCode("1234567").build();
        deviceList.add(device1);
        device2 = TrackedDeviceDAO.builder().id(2L).pinCode("2345678").build();
        deviceList.add(device2);
    }

    /**
     * Tests the createTrackedDevice method.
     * It checks if the response status code and body are correct.
     */
    @Test
    public void testCreateTrackedDevice() {
        when(deviceConfigurationService.createTrackedDevice(device1)).thenReturn(device1);
        ResponseEntity<TrackedDeviceDAO> response = trackedDeviceController.createTrackedDevice(device1);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(device1, response.getBody());
    }

    /**
     * Tests the getAllTrackedDevices method.
     * It checks if the response is equal to the deviceList variable.
     */
    @Test
    public void testGetAllTrackedDevices() {
        when(deviceConfigurationService.getAllTrackedDevices()).thenReturn(deviceList);
        List<TrackedDeviceDAO> response = trackedDeviceController.getAllTrackedDevices();
        assertEquals(deviceList, response);
    }

    /**
     * Tests the updateTrackedDevice method.
     * It checks if the response status code and body are correct.
     */
    @Test
    public void testUpdateTrackedDevice() {
        when(deviceConfigurationService.updateTrackedDevice(1L)).thenReturn(device1);
        ResponseEntity<TrackedDeviceDAO> response = trackedDeviceController.updateTrackedDevice(1L);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(device1, response.getBody());
    }

    /**
     * Tests the getTrackedDeviceById method.
     * It checks if the response status code and body are correct.
     */
    @Test
    public void testGetTrackedDeviceById() {;
        when(deviceConfigurationService.getTrackedDeviceById(1L)).thenReturn(device1);
        ResponseEntity<TrackedDeviceDAO> response = trackedDeviceController.getTrackedDeviceById(1L);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(device1, response.getBody());
    }

    /**
     * Tests the deleteTrackedDevice method.
     * It checks if the response status code is correct.
     */
    @Test
    public void testDeleteTrackedDevice() {
        ResponseEntity<Void> response = trackedDeviceController.deleteTrackedDevice(1L);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }
}
