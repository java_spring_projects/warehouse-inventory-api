package com.santigarcia.warehouseinventoryapi.service;

import com.santigarcia.warehouseinventoryapi.model.dao.TrackedDeviceDAO;

import java.util.List;

public interface DeviceConfigurationService {
    TrackedDeviceDAO createTrackedDevice(TrackedDeviceDAO trackedDevice);
    TrackedDeviceDAO updateTrackedDevice(Long id);
    List<TrackedDeviceDAO> getAllTrackedDevices();
    TrackedDeviceDAO getTrackedDeviceById(Long id);
    void deleteTrackedDevice(Long id);
}
