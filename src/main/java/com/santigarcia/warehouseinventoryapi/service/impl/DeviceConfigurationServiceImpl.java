package com.santigarcia.warehouseinventoryapi.service.impl;

import com.santigarcia.warehouseinventoryapi.model.dao.TrackedDeviceDAO;
import com.santigarcia.warehouseinventoryapi.model.entity.TrackedDeviceEntity;
import com.santigarcia.warehouseinventoryapi.repository.TrackedDeviceRepository;
import com.santigarcia.warehouseinventoryapi.service.DeviceConfigurationService;
import com.santigarcia.warehouseinventoryapi.util.exception.DeviceAlreadyExistsException;
import com.santigarcia.warehouseinventoryapi.util.exception.ResourceNotFoundException;
import com.santigarcia.warehouseinventoryapi.util.exception.trackeddeviceutils.TrackedDeviceUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 *  Service implementation that provides the CRUD operations for TrackedDeviceDAO
 *  using TrackedDeviceEntity from the persistence layer.
 *  Uses TrackedDeviceUtils to convert between DAO and Entity.
 */
@Service
public class DeviceConfigurationServiceImpl implements DeviceConfigurationService {
    @Autowired
    private TrackedDeviceRepository trackedDeviceRepository;

    /**
     * Creates a new tracked device.
     * @param trackedDevice The TrackedDeviceDAO object to create.
     * @return A TrackedDeviceDAO object representing the created device.
     * @throws DeviceAlreadyExistsException if the pinCode already exists in the database.
     */
    public TrackedDeviceDAO createTrackedDevice(TrackedDeviceDAO trackedDevice) {
        TrackedDeviceEntity existingDevice = trackedDeviceRepository.findByPinCode(new BigInteger(trackedDevice.getPinCode()));
        if (existingDevice != null) {
            throw new DeviceAlreadyExistsException("Device already exist.");
        }
        trackedDevice.setStatus(TrackedDeviceUtils.READY_STATUS);
        trackedDevice.setTemperature(-1);
        return TrackedDeviceUtils.toDAO(trackedDeviceRepository.save(TrackedDeviceUtils.toEntity(trackedDevice)));
    }

    /**
     * Updates the status and temperature of an existing tracked device.
     * @param id The id of the TrackedDeviceDAO object to update.
     * @return A TrackedDeviceDAO object representing the updated device.
     * @throws ResourceNotFoundException if the device with the specified id does not exist.
     */
    @Override
    public TrackedDeviceDAO updateTrackedDevice(Long id) {
        TrackedDeviceEntity existingDevice = trackedDeviceRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Device not found with id: " + id));

        existingDevice.setStatus(TrackedDeviceUtils.ACTIVE_STATUS_INT);
        existingDevice.setTemperature(TrackedDeviceUtils.configureTemperature());

        return TrackedDeviceUtils.toDAO(trackedDeviceRepository.save(existingDevice));
    }

    /**
     * Retrieves a list of all tracked devices in the system.
     * @return A list of TrackedDeviceDAO objects representing all tracked devices in the system.
     */
    @Override
    public List<TrackedDeviceDAO> getAllTrackedDevices() {
        List<TrackedDeviceDAO> daoDevices = new ArrayList<>();
        List<TrackedDeviceEntity> devices = trackedDeviceRepository.findAllOrderByPinCodeAsc();

        devices.forEach(device -> daoDevices.add(TrackedDeviceUtils.toDAO(device)));
        return daoDevices;
    }

    /**
     * Retrieves a tracked device by its id.
     * @param id The id of the TrackedDeviceDAO object to retrieve.
     * @return A TrackedDeviceDAO object representing the retrieved device.
     * @throws ResourceNotFoundException if the device with the specified id does not exist.
     */
    @Override
    public TrackedDeviceDAO getTrackedDeviceById(Long id) {
        TrackedDeviceEntity existingDevice = trackedDeviceRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Device not found with id: " + id));

        return TrackedDeviceUtils.toDAO(existingDevice);
    }

    /**
     * Deletes a tracked device by its id.
     * @param id The id of the TrackedDeviceDAO object to delete.
     * @throws ResourceNotFoundException if the device with the specified id does not exist.
     */
    @Override
    public void deleteTrackedDevice(Long id) {
        try {
            trackedDeviceRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e){
            throw new ResourceNotFoundException("Device not found with id: " + id);
        }
    }
}
