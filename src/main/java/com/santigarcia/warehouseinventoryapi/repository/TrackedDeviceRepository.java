package com.santigarcia.warehouseinventoryapi.repository;

import com.santigarcia.warehouseinventoryapi.model.entity.TrackedDeviceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface TrackedDeviceRepository extends JpaRepository<TrackedDeviceEntity, Long> {
    TrackedDeviceEntity findByPinCode(BigInteger pinCode);
    @Query("SELECT td FROM TrackedDeviceEntity td ORDER BY td.pinCode ASC")
    List<TrackedDeviceEntity> findAllOrderByPinCodeAsc();
}
