package com.santigarcia.warehouseinventoryapi.util.exception.trackeddeviceutils;

import com.santigarcia.warehouseinventoryapi.model.dao.TrackedDeviceDAO;
import com.santigarcia.warehouseinventoryapi.model.entity.TrackedDeviceEntity;

import java.math.BigInteger;
import java.util.Random;

public class TrackedDeviceUtils {
    public static final String ACTIVE_STATUS = "ACTIVE";
    public static final String READY_STATUS = "READY";
    public static final int ACTIVE_STATUS_INT = 1;
    public static final int READY_STATUS_INT = 0;
    public static final int MAX_TEMPERATURE = 10;
    public static TrackedDeviceDAO toDAO(TrackedDeviceEntity entity) {
        return TrackedDeviceDAO.builder()
                .id(entity.getId())
                .status(getStringStatusFromInt(entity.getStatus()))
                .temperature(entity.getTemperature())
                .pinCode(entity.getPinCode().toString())
                .build();
    }

    public static TrackedDeviceEntity toEntity(TrackedDeviceDAO dao) {
        return TrackedDeviceEntity.builder()
                .status(getIntStatusFromString(dao.getStatus()))
                .temperature(dao.getTemperature())
                .pinCode(new BigInteger(dao.getPinCode()))
                .build();
    }

    private static String getStringStatusFromInt(int deviceStatus) {
        return (deviceStatus == READY_STATUS_INT) ? READY_STATUS :
                (deviceStatus == ACTIVE_STATUS_INT) ? ACTIVE_STATUS : "";
    }

    private static int getIntStatusFromString(String deviceStatus) {
        return deviceStatus.equals(ACTIVE_STATUS) ? ACTIVE_STATUS_INT : 0;
    }

    public static int configureTemperature() {
        Random random = new Random();
        return random.nextInt(MAX_TEMPERATURE+1);
    }
}
