package com.santigarcia.warehouseinventoryapi.util.exception.handler;

import com.santigarcia.warehouseinventoryapi.util.exception.DeviceAlreadyExistsException;
import com.santigarcia.warehouseinventoryapi.util.exception.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * This class provides global exception handling for TrackedDevice-related exceptions.
 */
@ControllerAdvice
public class TrackedDeviceExceptionHandler {

    /**
     * Handles DeviceAlreadyExistsException and returns a Bad Request response with the exception message.
     * @param ex The exception to handle.
     * @return A Bad Request response with the exception message.
     */
    @ExceptionHandler(DeviceAlreadyExistsException.class)
    public ResponseEntity<String> handleDeviceAlreadyExistException(DeviceAlreadyExistsException ex) {
        return ResponseEntity.badRequest().body(ex.getMessage());
    }

    /**
     * Handles MethodArgumentNotValidException and returns a Bad Request response with the error messages.
     * @param ex The exception to handle.
     * @return A Bad Request response with the error messages.
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<String> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
        List<String> errorMessages = new ArrayList<>();
        for (FieldError error : fieldErrors) {
            errorMessages.add(error.getDefaultMessage());
        }
        String message = String.join(", ", errorMessages);
        return ResponseEntity.badRequest().body(message);
    }

    /**
     * Handles ResourceNotFoundException and returns a Bad Request response with the exception message.
     * @param ex The exception to handle.
     * @return A Bad Request response with the exception message.
     */
    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<String> handleResourceNotFoundExceptionException(ResourceNotFoundException ex) {
        return ResponseEntity.badRequest().body(ex.getMessage());
    }
}
