package com.santigarcia.warehouseinventoryapi.controller;

import com.santigarcia.warehouseinventoryapi.model.dao.TrackedDeviceDAO;
import com.santigarcia.warehouseinventoryapi.service.DeviceConfigurationService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * REST controller for managing tracked devices.
 */
@RestController
@RequestMapping("/trackedDevices")
public class TrackedDeviceController {
    @Autowired
    private DeviceConfigurationService trackedDeviceService;

    /**
     * Creates a new tracked device.
     *
     * @param trackedDevice The tracked device to create.
     * @return A response entity containing the created tracked device.
     */
    @PostMapping("/newDevice")
    public ResponseEntity<TrackedDeviceDAO> createTrackedDevice(
            @Valid @RequestBody TrackedDeviceDAO trackedDevice) {

        TrackedDeviceDAO newDevice = trackedDeviceService.createTrackedDevice(trackedDevice);
        return ResponseEntity.status(HttpStatus.CREATED).body(newDevice);
    }

    /**
     * Gets all tracked devices.
     *
     * @return A list of all tracked devices.
     */
    @GetMapping
    public List<TrackedDeviceDAO> getAllTrackedDevices() {
        return trackedDeviceService.getAllTrackedDevices();
    }

    /**
     * Updates the configuration of a tracked device.
     *
     * @param id The ID of the tracked device to update.
     * @return A response entity containing the updated tracked device.
     */
    @PutMapping("/configDevice/{id}")
    public ResponseEntity<TrackedDeviceDAO> updateTrackedDevice(
           @Valid @PathVariable(value = "id") Long id) {

        TrackedDeviceDAO updatedDevice = trackedDeviceService.updateTrackedDevice(id);
        return ResponseEntity.ok(updatedDevice);
    }

    /**
     * Gets a tracked device by ID.
     *
     * @param id The ID of the tracked device to retrieve.
     * @return A response entity containing the requested tracked device.
     */
    @GetMapping("/{id}")
    public ResponseEntity<TrackedDeviceDAO> getTrackedDeviceById(
            @Valid @PathVariable(value = "id") Long id) {
        return ResponseEntity.ok(trackedDeviceService.getTrackedDeviceById(id));
    }

    /**
     * Deletes a tracked device.
     *
     * @param id The ID of the tracked device to delete.
     * @return A response entity with no content.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTrackedDevice(@PathVariable Long id) {
        trackedDeviceService.deleteTrackedDevice(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
