package com.santigarcia.warehouseinventoryapi.model.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.math.BigInteger;

/**
 * Entity class for TrackedDevice. Represents the TrackedDevice object as it is persisted in the database.
 * */
@Entity
@Table(name = "tracked_device", uniqueConstraints = {@UniqueConstraint(columnNames = {"pin_code"})})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class TrackedDeviceEntity {

    /**
     * The unique identifier for the TrackedDeviceEntity object.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    /**
     * The current status of the TrackedDeviceEntity object.
     */
    @Column(name = "status")
    private int status;

    /**
     * The current temperature of the TrackedDeviceEntity object.
     */
    @Column(name = "temperature")
    private int temperature;

    /**
     * The unique pin code associated with the TrackedDeviceEntity object.
     */
    @Column(name = "pin_code")
    @NotNull
    private BigInteger pinCode;
}
