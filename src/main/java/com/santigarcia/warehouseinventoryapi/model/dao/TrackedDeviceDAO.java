package com.santigarcia.warehouseinventoryapi.model.dao;

import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Data Access Object class for Tracked Device entity. This class is used to represent and transfer
 *  tracked device data between different layers of the application.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TrackedDeviceDAO {

    /**
     * Unique identifier for the tracked device.
     */
    private Long id;

    /**
     * Pin code for the tracked device, consisting of 7 digits.
     */
    @NotEmpty(message = "Pin code cannot be empty")
    @Pattern(regexp = "\\d{7}", message = "Pin code must be a 7-digit number")
    private String pinCode;

    /**
     * Status of the tracked device.
     */
    private String status;

    /**
     * Temperature value of the tracked device.
     */
    private int temperature;
}



