package com.santigarcia.warehouseinventoryapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WarehouseInventoryApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(WarehouseInventoryApiApplication.class, args);
	}

}
