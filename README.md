# Warehouse Inventory API

## Getting started

- Clone the repository

```
git clone https://gitlab.com/java_spring_projects/warehouse-inventory-api.git
cd warehouse-inventory-api
```
## Initial devices on h2 database

- As soon as the project is started, the initial data found in the "import.sql" file is loaded, all the data is default devices
- Access to the in-memory h2 database is in the "application.properties" file:

```
url: http://localhost:8080/h2
jdbc url: jdbc:h2:mem:testdb
user: sa
pass:
```
- The username and password are the default

# How to use the API

### Base URL
```
http://localhost:8080/trackedDevices
```

### Endpoint 1 - Create a new tracked device

```
Method: POST
URL: http://localhost:8080/trackedDevices/newDevice
Request body: The tracked device to create with pinCode unique and not null
```
- Request example

```
{
    "pinCode": "8617229"
}
```

- Response example

```
{
    "id": 14,
    "pinCode": "8617229",
    "status": "READY",
    "temperature": -1
}
```
- Although the device is shown with the pin code and status parameters as strings, 
    in the database they are numbers, with status = 0 (READY) and status = 1 (ACTIVE).

### Endpoint 2 - Get all tracked devices

```
Method: GET
URL: http://localhost:8080/trackedDevices
```

- The response is a list of devices ordered by pin code.

### Endpoint 3 - Update the configuration of a tracked device

```
Method: PUT
URL: http://localhost:8080/trackedDevices/configDevice/{id}
PathVariable: id - The ID of the tracked device to update.
```

- Response example for id = 14: When configuring a device, the response will be the same with ACTIVE status 
    and the temperature as a random number between 0 and 10

```
{
    "id": 14,
    "pinCode": "8617229",
    "status": "ACTIVE",
    "temperature": 5
}
```

### Endpoint 4 - Get a tracked device by ID

```
Method: GET
URL: http://localhost:8080/trackedDevices/{id}
PathVariable: id - The ID of the tracked device to retrieve.
```

### Endpoint 5 - Delete a tracked device

```
Method: DELETE
URL: http://localhost:8080/trackedDevices/{id}
PathVariable: id - The ID of the tracked device to delete.
```

## Considerations

- An attempt has been made to follow the instructions for this test as indicated in the document:
  - Tests have been implemented until 80% coverage has been reached
  - The relevant validations have been implemented
  - An attempt has been made to keep a clean design
  
- To see the code coverage using jacoco, it can be accessed from the project root directory:

```
warehouseinventoryapi/target/site/jacoco/index.html
```

## Author

- Santiago Garcia
- Linkedin: https://www.linkedin.com/in/santiago-garcia-0b15a81a5/